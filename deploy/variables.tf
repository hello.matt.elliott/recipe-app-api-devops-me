variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "hello.matt.elliott@gmail.com"
}

variable "ami" {
  default = "amzn2-ami-hvm-2.0.*-x86_64-gp2"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "db_username" {
  description = "Username for the RDS pg instance"
}

variable "db_password" {
  description = "Password for the RDS pg instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "618756747842.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for Proxy"
  default     = "618756747842.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app "
}

variable "dns_zone_name" {
  description = "Domain name"
  default     = "featureflip.io"
}

variable "subdomain" {
  description = "Subdomain per env"
  type        = map(string)
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}